/* TypeFighter
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "typecursor.h"

class Player
{
public:
    static Color IdToColor(unsigned id)
    {
        switch (id)
        {
        case 0u: return Color::RED;
        case 1u: return Color::CHARTREUSE;
        case 2u: return Color::AZURE;
        case 3u: return Color::VIOLET;
        default: return Color::WHITE;
        }
    }

    Player(unsigned id);

    void SetCursor(TypeCursor* cursor) { cursor_= cursor; }

    unsigned GetId() const noexcept { return id_; }
    TypeCursor* GetCursor() const noexcept { return cursor_; }

private:
    unsigned id_;
    unsigned score_;
    Text* scoreText_;
    TypeCursor* cursor_;
};

#endif // PLAYER_H
