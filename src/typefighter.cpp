/* TypeFighter
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "typefighter.h"

TypeFighter::TypeFighter(Context* context): UIElement(context),
    joystickId_{ 0u },
    buttonMap_{},
    stickPositions_{ {}, {} },
    leftChars_{},
    rightChars_{},
    leftNums_{},
    rightNums_{},
    numCapMap_{},
    leftChar_{},
    rightChar_{},
    caps_{ false },
    space_{ false },
    numeral_{ false }
{
    leftChars_ = {
        { "ABC" },
        { "GHI" },
        { "M" },
        { "OPQ" },
        { "UVW" },
    };
    rightChars_ = {
        { "DEF" },
        { "JKL" },
        { "N" },
        { "RST" },
        { "XYZ" },
    };

    leftNums_ = {
        { ">]}" },
        { "<[{" },
        { "," },
        { ";(\"" },
        { ":)/" },
    };
    rightNums_ = {
        { "123" },
        { "456" },
        { "0" },
        { "789" },
        { "*+-" },
    };

    numCapMap_ = {
        { '0', '='},
        { '1', '~'},
        { '2', '@'},
        { '3', '#'},
        { '4', '$'},
        { '5', '%'},
        { '6', '^'},
        { '7', '&'},
        { '8', '_'},
        { '9', '|'},
        { '"', '\''},
        { '/', '\\'},
        { ',', '`'},
                 };

    SetAlignment(HA_CENTER, VA_CENTER);
    SharedPtr<XMLFile> layoutFile{ RES(XMLFile, "UI/TypeFighterLayout.xml") };
    LoadChildXML(layoutFile->GetRoot(), nullptr);

    MapButtonsToElements();
    CreateTextElements();

    SubscribeToEvent(E_JOYSTICKAXISMOVE, DRY_HANDLER(TypeFighter, HandleJoystickAxisMove));
    SubscribeToEvent(E_JOYSTICKBUTTONDOWN, DRY_HANDLER(TypeFighter, HandleJoystickButtonDown));
    SubscribeToEvent(E_JOYSTICKBUTTONUP, DRY_HANDLER(TypeFighter, HandleJoystickButtonUp));
//    SubscribeToEvent(E_JOYSTICKCONNECTED, DRY_HANDLER(TypeFighter, HandleJoystickConnected));
//    SubscribeToEvent(E_JOYSTICKDISCONNECTED, DRY_HANDLER(TypeFighter, HandleJoystickDisconnected));

    SubscribeToEvent(E_JOYSTICKCONNECTED, DRY_HANDLER(TypeFighter, HandleJoystickConnected));
    SubscribeToEvent(E_POSTUPDATE, DRY_HANDLER(TypeFighter, DelayedConstruct));
}

void TypeFighter::UpdateOpacity()
{
    if (joystickId_ < INPUT->GetNumJoysticks())
    {
        for (bool left: { true, false })
            UpdateHighlight(left);

        SetOpacity(1.f);
    }
    else
    {
        SetOpacity(.25f);
    }
}

void TypeFighter::DelayedConstruct(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateOpacity();
    SetNumeral(false);
    UnsubscribeFromEvent(E_POSTUPDATE);
}

void TypeFighter::HandleJoystickConnected(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateOpacity();
}

void TypeFighter::MapButtonsToElements()
{
    buttonMap_[CONTROLLER_BUTTON_START]         = GetChild("Start",         true);
    buttonMap_[CONTROLLER_BUTTON_BACK]          = GetChild("Select",        true);
    buttonMap_[CONTROLLER_BUTTON_A]             = GetChild("Cross",         true);
    buttonMap_[CONTROLLER_BUTTON_B]             = GetChild("Circle",        true);
    buttonMap_[CONTROLLER_BUTTON_X]             = GetChild("Square",        true);
    buttonMap_[CONTROLLER_BUTTON_Y]             = GetChild("Triangle",      true);
    buttonMap_[CONTROLLER_BUTTON_DPAD_UP]       = GetChild("Up",            true);
    buttonMap_[CONTROLLER_BUTTON_DPAD_DOWN]     = GetChild("Down",          true);
    buttonMap_[CONTROLLER_BUTTON_DPAD_LEFT]     = GetChild("Left",          true);
    buttonMap_[CONTROLLER_BUTTON_DPAD_RIGHT]    = GetChild("Right",         true);
    buttonMap_[CONTROLLER_BUTTON_LEFTSHOULDER]  = GetChild("LeftShoulder",  true);
    buttonMap_[CONTROLLER_BUTTON_RIGHTSHOULDER] = GetChild("RightShoulder", true);
    buttonMap_[CONTROLLER_BUTTON_LEFTSTICK]     = GetChild("LeftStick",     true);
    buttonMap_[CONTROLLER_BUTTON_RIGHTSTICK]    = GetChild("RightStick",    true);
}

void TypeFighter::CreateTextElements()
{
    for (bool left: { true, false })
    {
        UIElement* hat{ GetChild((left ? "LeftHat" : "RightHat"), true) };
        for (bool nums: { false, true })
        {
            UIElement* hatChars{ hat->CreateChild<UIElement>((nums ? "Numbers" : "Letters")) };
            hatChars->SetAlignment(HA_CENTER, VA_CENTER);
            const StringVector& chars{ (left ? (nums ? leftNums_  : leftChars_)
                                             : (nums ? rightNums_ : rightChars_)) };

            for (int r{ 0 }; r < chars.Size(); ++r)
            {
                const String row{ chars.At(r) };
                for (int c{ 0 }; c < row.Length(); ++c)
                {
                    const float angle{ 60.f * (r > 2 ? 1 - c : c - 1) };
                    const Vector2 pos{ (Vector2::DOWN * ((2 - r) * 47)).Rotated(angle) + Vector2::DOWN * 3.f };
                    const IntVector2 intPos{ VectorRoundToInt(pos) };

                    Text* letter{ CreateLetter(row.Substring(c, 1), hatChars) };
                    letter->SetPosition(intPos);
                    letter->AddTag((nums ? "Number" : "Letter"));

                    if (nums && !numeral_)
                        letter->SetVisible(false);
                }
            }

        }
    }

    UIElement* leftStick{ GetChild("LeftStick", true) };
    leftStick->SetVar("Key", KEY_BACKSPACE);
    Text* backspaceText{ CreateLetter("Backsp", leftStick) };
    backspaceText->SetFontSize(25.f);
    backspaceText->SetTextEffect(TE_STROKE);
    backspaceText->SetEffectColor(Color::BLACK);
    UIElement* rightStick{ GetChild("RightStick", true) };
    rightStick->SetVar("Key", KEY_DELETE);
    Text* deleteText{ CreateLetter("Delete", rightStick) };
    deleteText->SetFontSize(25.f);
    deleteText->SetTextEffect(TE_STROKE);
    deleteText->SetEffectColor(Color::BLACK);

    const auto circleButtons{ GetChild("CircleButtons", true)->GetChildren() };
    for (unsigned b{ 0u }; b < circleButtons.Size(); ++b)
    {
        UIElement* button{ circleButtons.At(b) };
        if (!button)
            return;

        String character;
        switch (b) {
        case 0: character = "✓"; button->SetVar("Key", KEY_RETURN); break;
        case 1: character = "."; button->SetVar("Key", KEY_PERIOD); break;
        case 2: character = "?"; button->SetVar("Key", KEY_QUESTION); break;
        case 3: character = "!"; button->SetVar("Key", KEY_EXCLAIM); break;
        default:
        break;
        }
        CreateLetter(character, button->GetParent())->SetPosition(button->GetPosition());
    }

    CreateLetter("#", GetChild("Select", true));
    CreateLetter(leftChar_ , GetChild("LeftShoulder", true));
    CreateLetter(rightChar_, GetChild("RightShoulder", true));

    const auto arrowButtons{ GetChild("DPad", true)->GetChildren() };
    for (unsigned b{ 0u }; b < arrowButtons.Size(); ++b)
    {
        UIElement* button{ arrowButtons.At(b) };
        if (!button)
            return;

        switch (b) {
        case 0: button->SetVar("Key", KEY_RIGHT); break;
        case 1: button->SetVar("Key", KEY_LEFT); break;
//        case 2: button->SetVar("Key", KEY_DOWN); break;
//        case 3: button->SetVar("Key", KEY_UP); break;
        default: break;
        }
    }
}

Text* TypeFighter::CreateLetter(const String& text, UIElement* parent)
{
    Text* letter{ parent->CreateChild<Text>() };
    letter->SetName(text);
    letter->SetAlignment(HA_CENTER, VA_CENTER);
    letter->SetColor(Color::BLACK);
    letter->SetFont(RES(Font, "Fonts/WhiteRabbitReloaded.ttf"));
    letter->SetFontSize(40.f);
    letter->SetText(text);
    letter->SetUseDerivedOpacity(true);

    if (numCapMap_.Contains(text.Front()))
    {
        letter->SetVar("Num", text);
        letter->SetVar("Cap", String{ numCapMap_[text.Front()] });
    }

    return letter;
}

void TypeFighter::SetNumeral(bool numeral)
{
    numeral_ = numeral;

    for (bool left: { true, false })
    {
        UIElement* hat{ GetChild((left ? "LeftHat" : "RightHat"), true) };
        for (bool nums: { false, true })
        {
            UIElement* hatChars{ hat->GetChild((nums ? "Numbers" : "Letters"), false) };
            for (UIElement* c: hatChars->GetChildren())
                c->SetVisible(nums ^ !numeral_);
        }
    }

    UpdateHighlight();
}

void TypeFighter::SetColor(const Color& color)
{
    for (UIElement* e: GetChild(0)->GetChildren())
    {
        e->SetColor(color);
        for (UIElement* f: e->GetChildren())
        {
            if (f->GetType() == Text::GetTypeStatic() && static_cast<Text*>(f)->GetText().Length() < 4)
                continue;

            f->SetColor(color);
            for (UIElement* g: f->GetChildren())
            {
                if (g->GetType() == Text::GetTypeStatic())
                    continue;

                g->SetColor(color);
                g->SetVisible(false);
            }
        }
    }
}

void TypeFighter::HandleJoystickAxisMove(StringHash /*eventType*/, VariantMap& eventData)
{
    const unsigned joystick{ eventData[JoystickButtonDown::P_JOYSTICKID].GetUInt() };
    if (joystick != joystickId_)
        return;

    const bool oldCaps{ caps_ };
    const bool oldSpace_{ space_ };
    const unsigned axis{ eventData[JoystickAxisMove::P_AXIS].GetUInt() };
    const float pos{ eventData[JoystickAxisMove::P_POSITION].GetFloat() };

    switch (axis)
    {
    case CONTROLLER_AXIS_LEFTX:  stickPositions_.first_ .x_ =  pos; break;
    case CONTROLLER_AXIS_LEFTY:  stickPositions_.first_ .y_ = -pos; break;
    case CONTROLLER_AXIS_RIGHTX: stickPositions_.second_.x_ =  pos; break;
    case CONTROLLER_AXIS_RIGHTY: stickPositions_.second_.y_ = -pos; break;
    case CONTROLLER_AXIS_TRIGGERLEFT:  caps_  = pos > .125f ; break;
    case CONTROLLER_AXIS_TRIGGERRIGHT: space_ = pos > .125f; break;
    default: return;
    }

    UpdateHighlight(axis < 2u);

    if (caps_ != oldCaps || oldSpace_ != space_)
    {
        for (bool left: { true, false })
        {
            Sprite* s{ GetChildStaticCast<Sprite>((left ? "LeftTrigger" : "RightTrigger"), true) };
            const int mul{ (left ? (caps_ - oldCaps) : (space_ - oldSpace_))};
            const int height{ s->GetImageRect().Height() * mul };
            const IntRect offset{ 0, height, 0, height };
            s->SetImageRect(s->GetImageRect() + offset);

            if (numeral_)
            {
                UIElement* hat{ GetChild((left ? "LeftHat" : "RightHat"), true) };
                UIElement* hatChars{ hat->GetChild("Numbers", false) };
                for (UIElement* c: hatChars->GetChildren())
                {
                    Text* text{ static_cast<Text*>(c) };
                    if (numCapMap_.Contains(text->GetVar("Num").GetString().Front()))
                    {
                        if (!caps_)
                            text->SetText(text->GetVar("Num").GetString());
                        else
                            text->SetText(text->GetVar("Cap").GetString());
                    }
                }
            }
        }
    }
}

void TypeFighter::UpdateHighlight()
{
    for (bool left: { false, true })
        UpdateHighlight(left);
}

void TypeFighter::UpdateHighlight(bool left)
{
    UIElement* highlight{ GetChild((left ? "LeftHat" : "RightHat"), true)->GetChild("Highlight", false) };
    const Vector2& sp{ (left ? stickPositions_.first_ : stickPositions_.second_) };
    const float offCenter{ sp.Length() / Lerp(1.f, M_SQRT2f,
                        Clamp(1.f - Abs(Abs(sp.x_) - Abs(sp.y_)) / Max(M_EPSILON, Max(sp.x_, sp.y_)), 0.f, 1.f)) };

    const bool out{ offCenter > 1/12.f };
    const bool outer{ offCenter >= 11/12.f };
    const int hexant{ (!out ? 0 : static_cast<int>(
                                  Round(sp.Angle(Vector2::UP) / 60.f)
                                  * (sp.Angle(Vector2::LEFT) > 90.f ? 1.f : -1.f))) };
    const float rotation{ (Equals(offCenter, 0.f) ? 0.f : 60.f * hexant) };


    const int column{ (!out ? 0 : (Abs(hexant) < 2 ? hexant + 1 : (Abs(hexant) == 3 ? 1 : hexant / 2 + 1))) };
    const int row{ (Abs(hexant) < 2 ? 2 - out - outer : 2 + out + outer)};
    (left ? leftChar_: rightChar_) = (left ? (numeral_ ? leftNums_  : leftChars_) :
                                             (numeral_ ? rightNums_ : rightChars_)).At(row).Substring(column, 1);

    for (UIElement* h: highlight->GetChildren())
    {
        Sprite* sector{ static_cast<Sprite*>(h) };
        if (sector->GetName() != "Center")
            sector->SetRotation(rotation);

        if (!out)
        {
            if (sector->GetName() == "Center")
                sector->SetVisible(true);
            else
                sector->SetVisible(false);
        }
        else if (!outer)
        {
            if (sector->GetName() == "Inner")
                sector->SetVisible(true);
            else
                sector->SetVisible(false);
        }
        else if (sector->GetName() == "Outer")
        {
            sector->SetVisible(true);
        }
        else
        {
            sector->SetVisible(false);
        }
    }


    for (bool left: { true, false })
    {
        String c{ (left ? leftChar_ : rightChar_) };
        UIElement* shoulder{ GetChild((left ? "LeftShoulder" : "RightShoulder"), true) };

        if (ContainsAlpha(c))
            c = String{ static_cast<char>(c.Front() + 32 * (!caps_)) };
        if (caps_ && numCapMap_.Contains(c.Front()))
            c = String{ numCapMap_[c.Front()] };

        shoulder->GetChildStaticCast<Text>(0)->SetText(c);
    }
}

void TypeFighter::HandleJoystickButtonDown(StringHash /*eventType*/, VariantMap& eventData)
{
    const unsigned joystick{ eventData[JoystickButtonDown::P_JOYSTICKID].GetUInt() };
    if (joystick != joystickId_)
        return;

    const unsigned button{ eventData[JoystickButtonDown::P_BUTTON].GetUInt() };
    if (buttonMap_.Contains(button))
    {
        UIElement* elem{ buttonMap_[button] };

        if (elem->GetType() == BorderImage::GetTypeStatic())
        {
            BorderImage* bi{ static_cast<BorderImage*>(elem) };
            const int height{ bi->GetImageRect().Height() };
            const IntRect offset{ 0, height, 0, height };
            bi->SetImageRect(bi->GetImageRect() + offset);
        }
        else if (elem->GetType() == Sprite::GetTypeStatic())
        {
            Sprite* s{ static_cast<Sprite*>(elem) };
            const int height{ s->GetImageRect().Height() };
            const IntRect offset{ 0, height, 0, height };
            s->SetImageRect(s->GetImageRect() + offset);
        }

        if (elem->GetParent()->GetName() == "CircleButtons"
         || elem->GetParent()->GetName() == "DPad"
         || elem->GetName().Contains("Stick"))
        {
            SendKeyDownEvent((Key)elem->GetVar("Key").GetUInt());
        }
    }

    if (button == CONTROLLER_BUTTON_LEFTSHOULDER)
        SendKeyDownEvent(StringToKey(leftChar_));
    else if (button == CONTROLLER_BUTTON_RIGHTSHOULDER)
        SendKeyDownEvent(StringToKey(rightChar_));
    else if (button == CONTROLLER_BUTTON_BACK)
        SetNumeral(!numeral_);
}

Key TypeFighter::StringToKey(const String& c) const
{
    if (c.Length() == 1)
    {
        if (ContainsAlpha(c))
            return (Key)(c.Front() + 32 * !caps_);
        if (caps_ && numCapMap_.Contains(c.Front()))
            return INPUT->GetKeyFromName(String{ numCapMap_[c.Front()]});
        else
            return INPUT->GetKeyFromName(c);
    }
    else
    {
        return KEY_UNKNOWN;
    }
}

bool TypeFighter::ContainsAlpha(const String& c) const
{
    return String{ "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" }.Find(c) != String::NPOS;
}

void TypeFighter::SendKeyDownEvent(Key k)
{
    VariantMap eventData{};

    eventData.Insert({ KeyDown::P_KEY, k });
    eventData.Insert({ KeyDown::P_SCANCODE, SCANCODE_UNKNOWN });
    eventData.Insert({ KeyDown::P_BUTTONS, MOUSEB_NONE });
    eventData.Insert({ KeyDown::P_QUALIFIERS, QUAL_NONE | (QUAL_CTRL * caps_) });
    eventData.Insert({ KeyDown::P_REPEAT, false });

    SendEvent(E_KEYDOWN, eventData);

    if (space_)
    {
        VariantMap spaceData{{ KeyDown::P_KEY, KEY_SPACE }};
        SendEvent(E_KEYDOWN, spaceData);
    }
}

void TypeFighter::HandleJoystickButtonUp(StringHash /*eventType*/, VariantMap& eventData)
{
    const unsigned joystick{ eventData[JoystickButtonDown::P_JOYSTICKID].GetUInt() };
    if (joystick != joystickId_)
        return;

    const unsigned button{ eventData[JoystickButtonDown::P_BUTTON].GetUInt() };
    if (buttonMap_.Contains(button))
    {
        UIElement* elem{ buttonMap_[button] };

        if (elem->GetType() == BorderImage::GetTypeStatic())
        {
            BorderImage* bi{ static_cast<BorderImage*>(elem) };
            const int height{ bi->GetImageRect().Height() };
            const IntRect offset{ 0, height, 0, height };
            bi->SetImageRect(bi->GetImageRect() - offset);
        }
        else if (elem->GetType() == Sprite::GetTypeStatic())
        {
            Sprite* s{ static_cast<Sprite*>(elem) };
            const int height{ s->GetImageRect().Height() };
            const IntRect offset{ 0, height, 0, height };
            s->SetImageRect(s->GetImageRect() - offset);
        }
    }
}

//void TypeFighter::HandleJoystickDisconnected(StringHash eventType, VariantMap& eventData) {}
