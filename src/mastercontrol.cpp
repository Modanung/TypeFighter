/* TypeFighter
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/UI/LineEdit.h"
#include "player.h"
#include "typefighter.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context *context): Application(context),
    scene_{ nullptr },
    lineEdit_{ nullptr }
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "TypeFighter.log";
    engineParameters_[EP_WINDOW_TITLE] = "TypeFighter";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";
    engineParameters_[EP_FULL_SCREEN] = false;
}

void MasterControl::Start()
{
    context_->RegisterSubsystem(this);
    context_->RegisterFactory<TypeFighter>();
    context_->RegisterFactory<TypeCursor>();

    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());

    INPUT->SetMouseMode(MM_FREE);

    CreateScene();

    SubscribeToEvent(E_JOYSTICKCONNECTED, DRY_HANDLER(MasterControl, HandleJoystickConnected));
}

void MasterControl::Stop()
{
//    engine_->DumpResources(true);
}

void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);


    UI* ui{ GetSubsystem<UI>() };
    UIElement* root{ ui->GetRoot() };
    root->SetDefaultStyle(RES(XMLFile, "UI/DefaultStyle.xml"));
    ui->SetScale(.666f * GRAPHICS->GetHeight() / 1080);

    CreateLineEdit();

    for (unsigned i{ 0u }; i < 4u; ++i)
    {
        TypeFighter* tf{ root->CreateChild<TypeFighter>() };
        Color col{ Player::IdToColor(i) };
        IntVector2 pos;
        switch (i)
        {
        case 0u: pos = { -GRAPHICS->GetHeight() * 3/4,  510 }; break;
        case 1u: pos = {  GRAPHICS->GetHeight() * 3/4,  510 }; break;
        case 2u: pos = { -GRAPHICS->GetHeight() * 3/4, -510 }; break;
        case 3u: pos = {  GRAPHICS->GetHeight() * 3/4, -510 }; break;
        default: pos = IntVector2::ZERO; break;
        }

        tf->SetJoystick(i);
        tf->SetColor(col);
        tf->SetPosition(pos);

        Player p{ i };
        TypeCursor* cursor{ lineEdit_->CreateChild<TypeCursor>() };
        p.SetCursor(cursor);
        cursor->SetColor(col);
        cursor->SetVisible(p.GetId() < INPUT->GetNumJoysticks());
        players_.Push(p);
    }
}

void MasterControl::CreateLineEdit()
{
    UI* ui{ GetSubsystem<UI>() };
    UIElement* root{ ui->GetRoot() };

    lineEdit_ = root->CreateChild<LineEdit>();
    lineEdit_->SetStyleAuto(RES(XMLFile, "UI/DefaultStyle.xml"));
    lineEdit_->GetTextElement()->SetFontSize(100.f);
    lineEdit_->SetAlignment(HA_CENTER, VA_CENTER);
    lineEdit_->SetSize(GRAPHICS->GetWidth() / ui->GetScale(), 128);
    lineEdit_->SetFocusMode(FM_NOTFOCUSABLE);
//    lineEdit_->SetFocus(true);
//    lineEdit_->GetCursor()->SetWidth(16);

    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(MasterControl, HandleKeyDown));
}

void MasterControl::HandleKeyDown(StringHash /*eventType*/, VariantMap& eventData)
{
    Object* sender{ GetEventSender() };
    if (sender->GetType() != TypeFighter::GetTypeStatic())
        return;

    TypeFighter* tf{ static_cast<TypeFighter*>(sender) };
    TypeCursor* cursor{ players_.At(tf->GetJoystickId()).GetCursor() };
    lineEdit_->SetCursorPosition(cursor->GetLocation());

    const unsigned k{ eventData[KeyDown::P_KEY].GetUInt() };
    const Key key{ (Key)k };
    QualifierFlags q{ (QualifierFlags)eventData[KeyDown::P_QUALIFIERS].GetUInt() };

    /*if (key == KEY_RETURN)
        lineEdit_->SetText({});
    else */
    if (key == KEY_BACKSPACE || key == KEY_DELETE)
        lineEdit_->OnKey(key, MOUSEB_NONE, q);
    else if (key < KEY_CAPSLOCK)
        lineEdit_->OnTextInput(String{ (char)k });
    else
    {
        switch(key)
        {
        default: break;
        case KEY_LEFT:  lineEdit_->OnKey(KEY_LEFT, MOUSEB_NONE, q); break;
        case KEY_RIGHT: lineEdit_->OnKey(KEY_RIGHT, MOUSEB_NONE, q); break;
//        case KEY_UP: break;
//        case KEY_DOWN: break;
        }
    }

    cursor->SetPosition(lineEdit_->GetCursor()->GetPosition());
    cursor->SetLocation(lineEdit_->GetCursorPosition());
}

void MasterControl::HandleJoystickConnected(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    for (const Player& p: players_)
        p.GetCursor()->SetVisible(p.GetId() < INPUT->GetNumJoysticks());
}
