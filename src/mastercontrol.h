/* TypeFighter
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

#define MC GetSubsystem<MasterControl>()

class TypeCursor;
class Player;

class MasterControl: public Application
{
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(Context* context);

    Scene* GetScene() const { return scene_; }

    // Setup before engine initialization. Modifies the engine paramaters.
    void Setup() override;
    // Setup after engine initialization.
    void Start() override;
    // Cleanup after the main loop. Called by Application.
    void Stop() override;
    void Exit();

private:
    void CreateScene();
    void CreateLineEdit();

    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    void HandleJoystickConnected(StringHash, VariantMap&);

    Scene* scene_;
    LineEdit* lineEdit_;
    Vector<Player> players_;
};

#endif // MASTERCONTROL_H
