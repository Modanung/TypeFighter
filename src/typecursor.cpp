/* TypeFighter
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "typecursor.h"


TypeCursor::TypeCursor(Context* context): BorderImage(context)
{
    SetSize(12, 96);
    SetTexture(RES(Texture2D, "UI/TypeFighter.png"));
    SetImageRect({ 576, 192, 608, 448 });
    SetVerticalAlignment(VA_CENTER);
    SetBlendMode(BLEND_ALPHA);
}

