HEADERS += \
    $$PWD/luckey.h \
    $$PWD/mastercontrol.h \
    $$PWD/player.h \
    $$PWD/typecursor.h \
    $$PWD/typefighter.h

SOURCES += \
    $$PWD/luckey.cpp \
    $$PWD/mastercontrol.cpp \
    $$PWD/player.cpp \
    $$PWD/typecursor.cpp \
    $$PWD/typefighter.cpp
