/* TypeFighter
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TYPEFIGHTER_H
#define TYPEFIGHTER_H

#include "luckey.h"


class TypeFighter: public UIElement
{
    DRY_OBJECT(TypeFighter, UIElement);

public:
    TypeFighter(Context* context);

    void MapButtonsToElements();
    void SetColor(const Color& color);
    void SetJoystick(unsigned id) { joystickId_ = id; }
    unsigned GetJoystickId() const { return joystickId_; }

private:
    void UpdateOpacity();
    void CreateTextElements();
    Text* CreateLetter(const String& text, UIElement* parent = nullptr);
    void UpdateHighlight();
    void UpdateHighlight(bool left);
    void SetNumeral(bool numeral);

    void DelayedConstruct(StringHash eventType, VariantMap& eventData);
    void HandleJoystickConnected(StringHash eventType, VariantMap& eventData);
    void HandleJoystickAxisMove(StringHash eventType, VariantMap& eventData);
    void HandleJoystickButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleJoystickButtonUp(StringHash eventType, VariantMap& eventData);

    Key StringToKey(const String& c) const;
    bool ContainsAlpha(const String& c) const;
    void SendKeyDownEvent(Key k);

    unsigned joystickId_;
    HashMap<unsigned, UIElement*> buttonMap_;
    Pair<Vector2, Vector2> stickPositions_;
    StringVector leftChars_;
    StringVector rightChars_;
    StringVector leftNums_;
    StringVector rightNums_;
    HashMap<char, char> numCapMap_;
    String leftChar_;
    String rightChar_;
    bool caps_;
    bool space_;
    bool numeral_;
};

#endif // TYPEFIGHTER_H
