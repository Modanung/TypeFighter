Original [White Rabbit](https://www.dafont.com/white-rabbit.font) font by Matthew Welch, modified with [FontForge](https://fontforge.org/en-US/) by Frode 魔大农 Lindeijer into _White Rabbit Reloaded_.

* License: [**CC0**](https://creativecommons.org/share-your-work/public-domain/cc0/)