include(src/TypeFighter.pri)

TARGET = typefighter

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

DISTFILES += \
    LICENSE_TEMPLATE
